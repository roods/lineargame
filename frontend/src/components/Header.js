import React, {Component} from "react";

export default class Header extends Component {

    constructor(props) {
        super(props);
    }

    changeLevel = () =>{
        this.props.changeLevel(this.props.gameInfo.level == 1 ? 2 : 1);
    }

    render() {
        return (
            <div className="header">

                <div id="topbanner">
                    {
                        this.props.gameInfo.mybalance >= 0 ?
                            <>
                                <span id={"level-btn"} onClick={this.changeLevel} className={"clickable level"+this.props.gameInfo.level}>Level: {this.props.gameInfo.level == 1 ? 'Easy' : 'Advanced'}</span>
                                <span id={"balance-btn"}>
                                    <span className={"balance-player"}>Balance: {this.props.gameInfo.mybalance}</span>
                                    <span className={"exit-game"} onClick={() => this.props.leaveGame()}>Exit game</span>
                                </span>
                            </>
                        :
                            <span id={"level-btn"} className={"level"+this.props.gameInfo.level}>Level: {this.props.gameInfo.level == 1 ? 'Easy' : 'Advanced'}</span>
                    }

                </div>
                {/*<div>*/}
                {/*    <label>Level</label>*/}
                {/*    {*/}
                {/*        this.props.gameInfo.mybalance >= 0 ? (*/}
                {/*            <select*/}
                {/*                onChange={this.changeLevel}>*/}
                {/*                <option*/}
                {/*                    selected={this.props.gameInfo.level == 1 ? true : false}*/}
                {/*                    value="1">Easy*/}
                {/*                </option>*/}
                {/*                <option*/}
                {/*                    selected={this.props.gameInfo.level == 2 ? true : false}*/}
                {/*                    value="2">Advanced*/}
                {/*                </option>*/}
                {/*            </select>*/}
                {/*        ) : (<span>{this.props.gameInfo.level == 1 ? 'Easy' : 'Advanced'}</span>)*/}
                {/*    }*/}
                {/*</div>*/}



                <div className={"players left" + (this.props.gameInfo.player1Addr.toUpperCase() == this.props.selectedAddress.toUpperCase() ? ' myself' : '')}>
                    <p>Player1</p>
                    <div className={"score-box"}>
                        <div className="score-box__score">
                            {this.props.gameInfo.player1Score}
                        </div>
                    </div>

                    <p>({this.props.gameInfo.player1Addr.substr(0, 4) + '...' + this.props.gameInfo.player1Addr.substr(-4)})</p>
                </div>
                <div className={"players right" + (this.props.gameInfo.player2Addr.toUpperCase() == this.props.selectedAddress.toUpperCase() ? ' myself' : '')}>
                    <p>Player2</p>
                    <div className={"score-box"}>
                        <div className="score-box__score">
                            {this.props.gameInfo.player2Score}
                        </div>
                    </div>
                    <p>({this.props.gameInfo.player2Addr.substr(0, 4) + '...' + this.props.gameInfo.player2Addr.substr(-4)})</p>
                </div>

                {/*{*/}
                {/*    this.props.gameInfo.mybalance >= 0 ? (*/}
                {/*        <div className="score-box">*/}
                {/*            <span>Balance</span>*/}
                {/*            <div className="score-box__score">{this.props.gameInfo.mybalance}</div>*/}
                {/*        </div>*/}
                {/*    ) : (<></>)*/}
                {/*}*/}
                {/*{*/}
                {/*    this.props.gameInfo.myBet >= 0 ? (*/}
                {/*        <div className="score-box">*/}
                {/*            <span>Bet</span>*/}
                {/*            <div className="score-box__score">{this.props.gameInfo.myBet}</div>*/}
                {/*        </div>*/}
                {/*    ) : (<></>)*/}
                {/*}*/}

            </div>
        );
    }
}

// const Header = ({ score, balance }) => {
//   return (
//     <div className="header">
//         <span>{player1Addr}</span> VS <span>{player2Addr}</span>
//       <div className="text">
//         <span>Rock</span>
//         <span>Paper</span>
//         <span>Scissors</span>
//       </div>
//       <div className="score-box">
//         <span>Score</span>
//         <div className="score-box__score">{score}</div>
//       </div>
//         {
//             balance >= 0 ? (
//                 <div className="score-box">
//                     <span>Balance</span>
//                     <div className="score-box__score">{balance}</div>
//                 </div>
//             ) : (<></>)
//         }
//
//     </div>
//   );
// };
//
// export default Header;
