import React, { Component } from 'react'
// import { Link } from "react-router-dom";
// import Triangle from "../images/bg-triangle.svg";

export default class Play extends Component {



    constructor(props) {
        super(props);

        this.state = {
            betAmount: Math.max(this.props.gameInfo.myBet, 0)
        }
    }

    setBetAmount = (e) => {

        this.setState(
            {
                betAmount: e.target.value
            }
        );


    }

    setMyChoice(item) {
        if(parseFloat(this.state.betAmount) > 0){
            this.props.setMyChoice(item, this.state.betAmount);

        }
        else{
            alert("You must specify an amount to bet!")
        }

    }

    render() {
        return (
            <>
                <div className={"play level"+this.props.gameInfo.level}>
                    {
                        this.props.gameInfo.mybalance >= 0 || this.props.gameInfo.canEnterGame == true ?
                            <div id={"betcontainer"}>
                                <label>Bet amount (ETH):</label>
                                <input type="text" defaultValue={this.state.betAmount} value={this.state.betAmount} name="name" onChange={this.setBetAmount} />
                                {
                                    this.props.gameInfo.fomoRemainingTime > 0 ? <label>Fomo in {Math.floor(this.props.gameInfo.fomoRemainingTime / 60)} min</label> : <></>
                                }

                            </div>
                        :
                            <></>
                    }


                    <div className="items">
                        {this.props.gameInfo.listChoices.map((item, index) => (
                            <div
                                key={item}
                                data-id={item}
                                id={"choice-container-"+item}
                                onClick={() => this.setMyChoice(item)}
                                className={"icon icon--" + item + (item == this.props.gameInfo.myChoice ? " selected" : "")}
                            >
                                <img src={require('../images/'+item+'.png')} />
                            </div>
                        ))}
                    </div>
                </div>
            </>
        );
    }
}