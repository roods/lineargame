async function main() {
    var rimraf = require("rimraf");

    rimraf(__dirname + "/../cache", function () { console.log("cache removed"); });

    if (network.name === "hardhat") {
        console.warn(
            "You are trying to deploy a contract to the Hardhat Network, which" +
            "gets automatically created and destroyed every time. Use the Hardhat" +
            " option '--network localhost'"
        );
    }

    const [deployer] = await ethers.getSigners();

    console.log("Deploying contracts with the account:", deployer.address);

    console.log("Account balance:", (await deployer.getBalance()).toString());

    const RockPaperScissor = await ethers.getContractFactory("RockPaperScissor");
    const rockPaperScissor = await RockPaperScissor.deploy();

    await rockPaperScissor.deployed();

    console.log("Contract address:", rockPaperScissor.address);

    // We also save the contract's artifacts and address in the frontend directory
    saveFrontendFiles(rockPaperScissor);
}

function saveFrontendFiles(rockPaperScissor) {
    const fs = require("fs");
    const contractsDir = __dirname + "/../frontend/src/contracts";

    if (!fs.existsSync(contractsDir)) {
        fs.mkdirSync(contractsDir);
    }

    fs.writeFileSync(
        contractsDir + "/contract-address.json",
        JSON.stringify({ RockPaperScissor: rockPaperScissor.address }, undefined, 2)
    );

    const RockPaperScissorArtifact = artifacts.readArtifactSync("RockPaperScissor");

    fs.writeFileSync(
        contractsDir + "/RockPaperScissor.json",
        JSON.stringify(RockPaperScissorArtifact, null, 2)
    );
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });