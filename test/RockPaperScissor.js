const { expect } = require("chai");

describe("RockPaperScissor contract", function () {
    let RockPaperScissor;
    let hardhatToken;
    let owner;
    let player1;
    let player2;
    const address0 = "0x0000000000000000000000000000000000000000";

    function calculateAmountForWinner(bet, decimalPrecision){

        let fullExpectedAmount = bet;
        let amountToFomoPool, amountForGameWinner;

        amountToFomoPool = fullExpectedAmount * 0.05 * decimalPrecision;
        amountForGameWinner = fullExpectedAmount * decimalPrecision - amountToFomoPool;

        // Making sure there is no decimal. If so, put it back to the pool
        let decimalAmountFomo = amountForGameWinner % decimalPrecision;

        amountToFomoPool    += decimalAmountFomo;
        amountForGameWinner -= decimalAmountFomo;

        amountForGameWinner = (amountForGameWinner / decimalPrecision) ;

        return [amountForGameWinner, amountToFomoPool];
    }

    beforeEach(async function () {
        // Get the ContractFactory and Signers here.
        RockPaperScissor = await ethers.getContractFactory("RockPaperScissor");
        [owner, player1, player2] = await ethers.getSigners();

        // Deploying contract
        hardhatToken = await RockPaperScissor.connect(owner).deploy();
    });

    describe("Deployment", function () {

        // If the callback function is async, Mocha will `await` it.
        it("Owner verification", async function () {

            // This test expects the owner variable stored in the contract to be equal to our Signer's owner.
            expect(await hardhatToken.owner()).to.equal(owner.address);
        });

    });

    describe("Playing", function () {

        it("Scenario1", async function () {

            let settings = {
                "timestampToDeadline"       : parseInt(await hardhatToken.connect(owner).timestampToDeadline()), // Retrieving the period to wait to win the Fomo Pool
                "lastTimestamp"             : 0, // Only for testing purpose, this field should be removed for live
                "testWinFomoPool"           : true, // Testing the scenario with a winner of the Fomo Pool
                "decimalPrecision"          : parseInt(await hardhatToken.connect(owner).decimalPrecision()) // Getting the decimal precision from the smart contract
            };

            let values  =   {

                "player1"   : {
                    "addr"              : "",
                    "bet"               : "",
                    "balance"           : "", // To reduce gas fee, each player has a balance within the smart contract
                    "move"              : "",
                    "selectedlevel"     : ""
                },
                "player2"   : {
                    "addr"              : "",
                    "bet"               : "",
                    "balance"           : "", // To reduce gas fee, each player has a balance within the smart contract
                    "move"              : "",
                    "selectedlevel"     : ""
                },
                "level"                 : 1,
                "betSameAmount"         : 0
            };

            let fomoPool = {
                "totaltimestampToDeadline"  : 0,
                "fomoTimer"                 : 0,
                "fomoPool"                  : 0,
                "amountForGameWinner"       : 0,
                "amountToFomoPool"          : 0,
                "amountPoolWinner"          : 0
            }



            // ------------------ GAME1 / PART1 ------------------

            /*

                Player1 joins the game, make a transfer from his wallet to bet and place a move all at once
                Player2 joins the game, make a transfer from his wallet to bet and place a move all at once
                They both play the same move
                It's a draw and both get their bet back in the balance

             */

            // Parameters of player1 for the game
            values.player1.addr             = player1.address.toString();
            values.player1.bet              = parseInt(ethers.utils.parseEther("1.0"));
            values.player1.balance          = 0;
            values.player1.move             = "ROCK";
            values.player1.selectedlevel    = 1;

            // If one player bet more than the other, making sure that the same amount is accounted for both
            values.betSameAmount = Math.min(parseInt(values.player1.bet), parseInt(values.player2.bet));

            // Interacting with the smartcontract
            // To reduce gas fee, multiple actions take place in one call. In this case, player1 joins the game, make a transfer from his wallet to bet and place a move all at once
            await hardhatToken.connect(player1).placeMove(values.player1.move, values.player1.selectedlevel, {
                value: values.player1.bet.toString()
            });


            // let [test1, test2] = await hardhatToken.connect(owner).getPlayers();
            // console.log(test1, test2);
            // let _balance = await hardhatToken.connect(owner).getBalance();

                // "lastTimestamp" helps to calculate the fomotimer in all the games
            settings.lastTimestamp = parseInt(await hardhatToken.connect(owner).lastTimestamp());

            // Verifying values for player1
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "bet")).to.equal(values.player1.bet.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "balance")).to.equal(values.player1.balance.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "move")).to.equal(values.player1.move);

            // Verifying values for player2
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "bet")).to.equal(values.player2.bet.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "balance")).to.equal(values.player2.balance.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "move")).to.equal(values.player2.move);

            // Verifying level of the game
            expect((await hardhatToken.connect(owner).level()).toString()).to.equal(values.player1.selectedlevel.toString());
            // Storing the current level of the game
            values.level = values.player1.selectedlevel;

            // The first fomotimer is the timestamp of the block when the fomopool got updated + the period extension
            fomoPool.fomoTimer = settings.lastTimestamp + settings.timestampToDeadline;
            // "totaltimestampToDeadline" helps to calculate how long the program should wait to verify the scenario with a winner of the fomopool
            fomoPool.totaltimestampToDeadline += settings.timestampToDeadline;

            // verifying fomotimer
            expect((await hardhatToken.connect(owner).fomoTimer()).toString()).to.equal(fomoPool.fomoTimer.toString());
            // verifying the last player is player1. When the fomotimer is up, the last player is the one winning the fomopool
            expect((await hardhatToken.connect(owner).lastPlayer()).toString()).to.equal(values.player1.addr.toString());



            // ------------------ GAME1 / PART2 ------------------


            // Parameters of player2 for the game
            values.player2.addr             = player2.address.toString();
            values.player2.bet              = parseInt(ethers.utils.parseEther("2.0"));
            values.player2.balance          = 0;
            values.player2.move             = "ROCK";
            values.player2.selectedlevel    = 1;

            // If one player bet more than the other, making sure that the same amount is accounted for both
            values.betSameAmount = Math.min(parseInt(values.player1.bet), parseInt(values.player2.bet));

            // Interacting with the smartcontract
            await hardhatToken.connect(player2).placeMove(values.player2.move, values.player2.selectedlevel, {
                value: values.player2.bet.toString()
            });

            // Both players played the same move, each player get back their bet in the balance
            // Expected parameters of player1 after executing the smart contract
            values.player1.balance          = values.player1.bet;
            values.player1.bet              = 0;
            values.player1.move             = "";

            // Expected parameters of player2 after executing the smart contract
            values.player2.balance          = values.player2.bet;
            values.player2.bet              = 0;
            values.player2.move             = "";

            // Verifying values for player1
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "bet")).to.equal(values.player1.bet.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "balance")).to.equal(values.player1.balance.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "move")).to.equal(values.player1.move);

            // Verifying values for player2
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "bet")).to.equal(values.player2.bet.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "balance")).to.equal(values.player2.balance.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "move")).to.equal(values.player2.move);

            // Verifying level of the game
            expect(await hardhatToken.connect(owner).level()).to.equal(values.level);


            fomoPool.fomoTimer += settings.timestampToDeadline;
            fomoPool.totaltimestampToDeadline += settings.timestampToDeadline;
            expect((await hardhatToken.connect(owner).fomoTimer()).toString()).to.equal(fomoPool.fomoTimer.toString());
            expect((await hardhatToken.connect(owner).lastPlayer()).toString()).to.equal(values.player2.addr.toString());


            // ------------------ GAME2 / PART1 ------------------

            /*

                Because of the previous draw, both players now have their balance populated
                For this game, both players will bet from the balance

                In this game, the level changed and player2 is the winner

             */

            // Parameters of player1
            values.player1.bet              = parseInt(ethers.utils.parseEther("0.7"));
            values.player1.balance          = values.player1.balance - values.player1.bet;
            values.player1.move             = "ROCK";
            values.player1.selectedlevel    = 2;

            values.betSameAmount = Math.min(parseInt(values.player1.bet), parseInt(values.player2.bet));

            // Interacting with the smartcontract
            await hardhatToken.connect(player1).placeMoveAndBetFromBalance(values.player1.move, values.player1.selectedlevel, values.player1.bet.toString());

            expect(await hardhatToken.connect(owner).level()).to.equal(values.player1.selectedlevel);
            values.level = values.player1.selectedlevel;

            // Verifying values for player1
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "bet")).to.equal(values.player1.bet.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "balance")).to.equal(values.player1.balance.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "move")).to.equal(values.player1.move);

            // Verifying values for player2
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "bet")).to.equal(values.player2.bet.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "balance")).to.equal(values.player2.balance.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "move")).to.equal(values.player2.move);

            fomoPool.fomoTimer += settings.timestampToDeadline;
            fomoPool.totaltimestampToDeadline += settings.timestampToDeadline;

            expect((await hardhatToken.connect(owner).fomoTimer()).toString()).to.equal(fomoPool.fomoTimer.toString());
            expect((await hardhatToken.connect(owner).lastPlayer()).toString()).to.equal(values.player1.addr.toString());



            // ------------------ GAME2 / PART2 ------------------

            // Parameters of player2
            values.player2.bet              = parseInt(ethers.utils.parseEther("1.7"));
            values.player2.balance          = values.player2.balance - values.player2.bet;
            values.player2.move             = "PAPER";
            values.player2.selectedlevel    = 1;

            // Player2 didn't notice the level changed, he tried to play on level1
            if(values.player2.selectedlevel != values.level){
                try {
                    // Interacting with the smartcontract
                    await hardhatToken.connect(player2).placeMoveAndBetFromBalance(values.player2.move, values.player2.selectedlevel, values.player2.bet.toString());
                }
                catch (error){
                    // Making sure we have an error message regarding the incorrect selected level
                    expect(error.message).to.equal("VM Exception while processing transaction: reverted with reason string 'RPSA1'");
                }

                // Player2 select the level2 as well
                values.player2.selectedlevel    = 2;
            }

            values.betSameAmount = Math.min(parseInt(values.player1.bet), parseInt(values.player2.bet));
            // Interacting with the smartcontract
            await hardhatToken.connect(player2).placeMoveAndBetFromBalance(values.player2.move, values.player2.selectedlevel, values.player2.bet.toString());

            // Parameters of player1 after losing the game
            values.player1.bet              = 0;
            values.player1.move             = "";

            // Verifying values for player1
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "bet")).to.equal(values.player1.bet.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "balance")).to.equal(values.player1.balance.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "move")).to.equal(values.player1.move);

            // Getting the amount earned by the winner after the cut for the fomopool
            [fomoPool.amountForGameWinner, fomoPool.amountToFomoPool] = calculateAmountForWinner(values.betSameAmount * 2, settings.decimalPrecision);

            // Parameters of player2 after winning the game
            values.player2.balance = (
                    fomoPool.amountForGameWinner + // Winning amount for the game
                    (parseInt(values.player2.bet) - values.betSameAmount) + // Transfer back the extra amount bet if any. In this case 1 Ether as player1 bet 0.7 but player2 bet 1.7
                    parseInt(values.player2.balance) // Current amount in player2's balance
            ); // 2630000000000000000
            values.player2.bet      = 0;
            values.player2.move     = "";

            // Verifying values for player2
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "bet")).to.equal(values.player2.bet.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "balance")).to.equal(values.player2.balance.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "move")).to.equal(values.player2.move);

            // Verifying amount in the fomopool
            expect((await hardhatToken.connect(owner).fomoPool()).toString()).to.equal(fomoPool.amountToFomoPool.toString());

            fomoPool.fomoTimer += settings.timestampToDeadline;
            fomoPool.totaltimestampToDeadline += settings.timestampToDeadline;
            expect((await hardhatToken.connect(owner).fomoTimer()).toString()).to.equal(fomoPool.fomoTimer.toString());
            expect((await hardhatToken.connect(owner).lastPlayer()).toString()).to.equal(values.player2.addr);


            // ------------------ GAME3 / PART1 ------------------

            /*

                Both players will now place a bet that won't trigger the  fomotimer's extension
                If "testWinFomoPool" is set to true in the "settings" variable, the program will wait for the fomotimer to expire. The last player will receive the full

                Player1 will win the game but player2 will win the fomopool

             */

            let betBelowFomoPool10Percent = fomoPool.amountToFomoPool * 0.09 / settings.decimalPrecision;

            values.player1.bet              = betBelowFomoPool10Percent;
            values.player1.balance          = values.player1.balance - values.player1.bet;
            values.player1.move             = "SPOCK";
            values.player1.selectedlevel    = 2;

            values.betSameAmount = Math.min(parseInt(values.player1.bet), parseInt(values.player2.bet));
            // Interacting with the smartcontract
            await hardhatToken.connect(player1).placeMoveAndBetFromBalance(values.player1.move, values.player1.selectedlevel, values.player1.bet.toString());

            expect(await hardhatToken.connect(owner).level()).to.equal(values.player1.selectedlevel);

            // Verifying values for player1
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "bet")).to.equal(values.player1.bet.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "balance")).to.equal(values.player1.balance.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "move")).to.equal(values.player1.move);

            // Verifying values for player2
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "bet")).to.equal(values.player2.bet.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "balance")).to.equal(values.player2.balance.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "move")).to.equal(values.player2.move);

            // Making sure that the fomotimer didn't change and the last player is still player2
            expect((await hardhatToken.connect(owner).fomoTimer()).toString()).to.equal(fomoPool.fomoTimer.toString());
            expect((await hardhatToken.connect(owner).lastPlayer()).toString()).to.equal(values.player2.addr);


            // ------------------ GAME3 / PART2 ------------------

            // If true, waiting for the fomotimer to expire
            if(settings.testWinFomoPool == true){
                // Adding a markup to make sure the time is up
                fomoPool.totaltimestampToDeadline = fomoPool.totaltimestampToDeadline * 1.5;
                console.log('Waiting for ....', fomoPool.totaltimestampToDeadline / 60, 'minutes' );

                // Changing the program's timeout with an extra markup
                this.timeout(fomoPool.totaltimestampToDeadline * 1.5 * 1000);

                let _timestampFinishWait = Date.now() + fomoPool.totaltimestampToDeadline * 1000;

                // setTimeout features doesn't work here, this is an alternative
                while(Date.now() < _timestampFinishWait){}
                fomoPool.amountPoolWinner = fomoPool.amountToFomoPool / settings.decimalPrecision;
            }

            // Parameters of player2
            values.player2.bet              = betBelowFomoPool10Percent;
            values.player2.balance          = values.player2.balance - values.player2.bet;
            values.player2.move             = "ROCK";
            values.player2.selectedlevel    = 2;



            values.betSameAmount = Math.min(parseInt(values.player1.bet), parseInt(values.player2.bet));
            // Interacting with the smartcontract
            await hardhatToken.connect(player2).placeMoveAndBetFromBalance(values.player2.move, values.player2.selectedlevel, values.player2.bet.toString());

            [fomoPool.amountForGameWinner, fomoPool.amountToFomoPool] = calculateAmountForWinner(values.betSameAmount * 2, settings.decimalPrecision);

            // Parameters of player1 after winning the game
            values.player1.bet              = 0;
            values.player1.balance          = values.player1.balance + fomoPool.amountForGameWinner;
            values.player1.move             = "";

            // Parameters of player2 after losing the game
            values.player2.bet              = 0;
            values.player2.move             = "";

            if(settings.testWinFomoPool == true) {

                // Adding the fomopool to player2's balance
                values.player2.balance += fomoPool.amountPoolWinner;

                // resetting fomotimer
                fomoPool.fomoTimer = 0;

                // Verify parameters for fomopool
                expect((await hardhatToken.connect(owner).fomoTimer()).toString()).to.equal(fomoPool.fomoTimer.toString());
                expect((await hardhatToken.connect(owner).lastPlayer()).toString()).to.equal(address0);
            }

            // Verifying values for player1
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "bet")).to.equal(values.player1.bet.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "balance")).to.equal(values.player1.balance.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player1.address, "move")).to.equal(values.player1.move);

            // Verifying values for player2
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "bet")).to.equal(values.player2.bet.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "balance")).to.equal(values.player2.balance.toString());
            expect(await hardhatToken.connect(owner).getPlayerInfo(player2.address, "move")).to.equal(values.player2.move);




        });




    });

});