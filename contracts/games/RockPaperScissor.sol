pragma solidity 0.8.7;
// SPDX-License-Identifier: MIT

import "../properties/TwoPlayersGame.sol";
//import "hardhat/console.sol";

contract RockPaperScissor is TwoPlayersGame{

    // ------------------------ STRUCTURES ------------------------

    struct finalOutcome {
        address payable player1_addr;
        uint256 player1_bet;
        string  player1_move;
        address payable player2_addr;
        uint256 player2_bet;
        string  player2_move;
        uint256 won_amount;
        string  game_winner;
        uint152 game_level;
        uint256 blocktime;
        uint256 fomo_timer;
        uint256 fomo_poolAmount;
        string  fomo_winner;
        uint256 fomo_wonAmount;
    }

    // ------------------------ PUBLIC ------------------------

    uint256 public fomoTimer    = 0;
    uint256 public fomoPool     = 0;
    uint152 public level;


    // ------------------------ PROTECTED ------------------------

    mapping(string => uint152) choices;

    string[5] listChoices;
    uint152 public decimalPrecision = 100;              //TODO: remove public when deploying live
        uint256 public timestampToDeadline = 1 * 60 * 60;
//    uint256 public timestampToDeadline = 1 * 60 / 6;    //TODO: When deploying live, comment current line and uncomment the previous one
    address public lastPlayer;                          //TODO: remove public when deploying live

    mapping(string => mapping (string => uint152)) outcomes;


    // ------------------------ EVENTS ------------------------

    event allPreviousGames(finalOutcome);

    // ------------------------ INIT ------------------------

    constructor()
    {

        // The totalSupply is assigned to transaction sender, which is the account
        // that is deploying the contract.
//        balances[msg.sender] = totalSupply;

        owner = msg.sender;
        gameStatus = statusState.WAITING_PLAYERS;

        // Setting all the choices possible
        choices["ROCK"]     = 1;
        choices["PAPER"]    = 2;
        choices["SCISSOR"]  = 3;
        choices["LIZARD"]   = 4;
        choices["SPOCK"]    = 5;

        // Init rules of the game
        outcomes    ["ROCK"]   ["ROCK"]       = 0 ;
        outcomes    ["ROCK"]   ["PAPER"]      = 2 ;
        outcomes    ["ROCK"]   ["SCISSOR"]    = 1 ;
        outcomes    ["ROCK"]   ["LIZARD"]     = 1 ;
        outcomes    ["ROCK"]   ["SPOCK"]      = 2 ;
        outcomes    ["PAPER"]  ["ROCK"]       = 1 ;
        outcomes    ["PAPER"]  ["PAPER"]      = 0 ;
        outcomes    ["PAPER"]  ["SCISSOR"]    = 2 ;
        outcomes    ["PAPER"]  ["LIZARD"]     = 2 ;
        outcomes    ["PAPER"]  ["SPOCK"]      = 1 ;
        outcomes    ["SCISSOR"] ["ROCK"]      = 2 ;
        outcomes    ["SCISSOR"] ["PAPER"]     = 1 ;
        outcomes    ["SCISSOR"] ["SCISSOR"]   = 0 ;
        outcomes    ["SCISSOR"] ["LIZARD"]    = 1 ;
        outcomes    ["SCISSOR"] ["SPOCK"]     = 2 ;
        outcomes    ["LIZARD"] ["ROCK"]       = 2 ;
        outcomes    ["LIZARD"] ["PAPER"]      = 1 ;
        outcomes    ["LIZARD"] ["SCISSOR"]    = 2 ;
        outcomes    ["LIZARD"] ["LIZARD"]     = 0 ;
        outcomes    ["LIZARD"] ["SPOCK"]      = 2 ;
        outcomes    ["SPOCK"]  ["ROCK"]       = 1 ;
        outcomes    ["SPOCK"]  ["PAPER"]      = 2 ;
        outcomes    ["SPOCK"]  ["SCISSOR"]    = 1 ;
        outcomes    ["SPOCK"]  ["LIZARD"]     = 2 ;
        outcomes    ["SPOCK"]  ["SPOCK"]      = 0 ;

        super.init();

        changeLevel(1);

    }

    // ------------------------ MODIFIERS ------------------------


    // ------------------------ TEST INTERFACES ------------------------


    // ------------------------ INTERFACES ------------------------

    // Getting the list of all the choices for the current level
    function getLevel()
    public
    view
    returns(uint152, string[5] memory)
    {

        return (level, listChoices);
    }

//    // Placing a bet using the amount available in the player's balance
//    function betFromBalance(uint256 _amount)
//    _isPlayer
//    public
//    {
//        super.betFromBalance(_amount);
//
//        updateFomoPool_act();
//        if(players[0].bet > 0 && players[1].bet > 0){
//            play_act();
//        }
//    }

//    // Topup balance
//    function addBalance(boolEnum _useAmountToBet)
//    payable
//    public
//    {
//        if(players[0].addr != msg.sender && players[1].addr != msg.sender){
//            super.enterGame();
//        }
//        if (msg.value > msg.sender.balance) {revert();}
//        super.addBalance(_useAmountToBet);
//        updateFomoPool_act();
//
//    }

    // Placing a move
    function placeMove(string memory _move, uint152 _lvl)
    _checkStatus
    payable
    public
    {
//        if (msg.value > msg.sender.balance) {revert();}

        // Making sure the move is one of the possibilities or revert
        string memory _verifiedMove = checkMove(_move, true);

        // If the player is not already in the game, we let him in
        if(players[0].addr != msg.sender && players[1].addr != msg.sender){
            super.enterGame();
        }

        // Change level if different and possible
        changeLevel(_lvl);

        // Topping up balance if the player is sending funds
        if(msg.value > 0){

            // Adding funds to the balance and place them in the bet
            super.addBalance(boolEnum.YES);
            // Update fomopool
            updateFomoPool_act();
        }

        // Placing the move for the player
        super._placeMove_act(_verifiedMove);

        // If both players bet already, let's find the outcome
        if(players[0].bet > 0 && players[1].bet > 0){
            play_act();
        }

    }

    // Placing a move and bet from the balance
    function placeMoveAndBetFromBalance(string memory _move, uint152 _lvl, uint256 _amountToBet)
    _isPlayer
    public
    {
        // Making sure the move is one of the possibilities or revert
        string memory _verifiedMove = checkMove(_move, true);

        // Change level if different and possible
        changeLevel(_lvl);

        // Placing the move for the player
        super._placeMove_act(_verifiedMove);

        // Update bet and balance for the player
        super.betFromBalance(_amountToBet);


        updateFomoPool_act();
        if(players[0].bet > 0 && players[1].bet > 0){
            play_act();
        }

    }



    // ------------------------ ACTIONS ------------------------

    function changeLevel(uint152 _lvl)
    _isPlayerOrOwner
    public
    {
        if(level != _lvl){
            require(players[0].bet == 0 && players[1].bet == 0, "RPSA1"); //Level cannot be changed mid-game.

            level = _lvl;
            // Setting choices according to the level
            delete listChoices[3];
            delete listChoices[4];

            listChoices[0] = "ROCK";
            listChoices[1] = "PAPER";
            listChoices[2] = "SCISSOR";

            if(level == 2){
                listChoices[3] = "LIZARD";
                listChoices[4] = "SPOCK";
            }
        }

    }

    function updateFomoPool_act()
    _isPlayer
    internal
    {

        uint256 _amountBet = 0;

        // Getting the bet to calculate the percentage for the fomopool
        if(msg.sender == players[0].addr){
            _amountBet = players[0].bet;
        }
        else if(msg.sender == players[1].addr){
            _amountBet = players[1].bet;
        }

        // Init fomotimer
        if(fomoTimer == 0 && _amountBet > 0){
            lastTimestamp = block.timestamp;
            fomoTimer = lastTimestamp + timestampToDeadline;
            lastPlayer = msg.sender;

        }
        else if(fomoTimer > block.timestamp){

            // Update fomotimer
            if(_amountBet * decimalPrecision >= fomoPool * 10 / 100 && lastPlayer != msg.sender){

                lastPlayer = msg.sender;
                fomoTimer += timestampToDeadline;

            }

        }
        // Last player receive the content of the pool
        else{

            uint256 _decimalAmountFomo;
            uint256 _amountFromFomoPool = 0;

            // Eventhough the full amount of the pool is added to the winning prize, we still can have a decimal when dividing by the precision markup
            // Instead of losing it definitively, we add it back to the pool
            _decimalAmountFomo = (fomoPool % decimalPrecision);
            if(_decimalAmountFomo > 0){

                _amountFromFomoPool = ((fomoPool - _decimalAmountFomo) / decimalPrecision);
                fomoPool = _decimalAmountFomo;
            }
            else{

                _amountFromFomoPool = fomoPool / decimalPrecision;
                fomoPool = 0;
            }

            // Resetting fomo timer to zero
            fomoTimer = 0;

            if(lastPlayer == players[0].addr){

                players[0].balance += _amountFromFomoPool;
            }
            else if(lastPlayer == players[1].addr){

                players[1].balance += _amountFromFomoPool;
            }

            lastPlayer = address(0);

        }
    }

    function play_act()
    _checkStatus
    noReentrant
    internal
    {
        require(players[0].addr != address(0)  && players[1].addr != address(0), "RPSA2"); //You need to have 2 players to play
        require(players[0].bet > 0  && players[1].bet > 0, "RPSA3"); //Both players need to bet to play

        // Making sure the move sent is one of the possibilities
        string memory player1Move = checkMove(players[0].move, true);
        string memory player2Move = checkMove(players[1].move, true);

        finalOutcome memory game_outcome;

        game_outcome.player1_addr       = players[0].addr;
        game_outcome.player1_bet        = players[0].bet;
        game_outcome.player1_move       = players[0].move;
        game_outcome.player2_addr       = players[1].addr;
        game_outcome.player2_bet        = players[1].bet;
        game_outcome.player2_move       = players[1].move;

        game_outcome.game_level         = level;
        game_outcome.blocktime          = block.timestamp;
        game_outcome.fomo_timer         = fomoTimer;
        game_outcome.fomo_poolAmount    = fomoPool;


        // Making sure that each player is not getting more than what he has bet. Extra amount sent back to the balance
        if(players[1].bet > players[0].bet){
            players[1].balance  += players[1].bet - players[0].bet;
            players[1].bet      = players[0].bet;
        }
        else if(players[0].bet > players[1].bet){
            players[0].balance  += players[0].bet - players[1].bet;
            players[0].bet      = players[1].bet;
        }

        // 0= draw; 1= players[0] wins; 2= players[1] wins
        uint152 _result = outcomes[player1Move][player2Move];

        // It s a draw, everybody get their bet amount
        if(_result == 0){
            game_outcome.game_winner    = '';
            game_outcome.won_amount     = 0;

            players[0].balance += players[0].bet;
            players[1].balance += players[1].bet;
        }
        else{

            uint256 _initialAmountForWinner = players[0].bet + players[1].bet;
            uint256 _amountForFomoPool;
            uint256 _decimalAmountFomo;
            uint256 _amountForWinner = 0;


            game_outcome.fomo_wonAmount = 0;

            _amountForFomoPool  = _initialAmountForWinner * decimalPrecision * 5 / 100;
            _amountForWinner    = (_initialAmountForWinner * decimalPrecision) - _amountForFomoPool;

            // Instead of losing the decimals, we just add it back to the pool
            _decimalAmountFomo = (_amountForWinner % decimalPrecision) ;

            _amountForFomoPool += _decimalAmountFomo;

            _amountForWinner    -= _decimalAmountFomo;
            _amountForWinner    = _amountForWinner / decimalPrecision;


            fomoPool += _amountForFomoPool;
            game_outcome.won_amount = _amountForWinner;

            // players[0] wins
            if(_result == 1){
                players[0].balance     += _amountForWinner;
                players[0].score       += 1;
            }

            // players[1] wins
            else if(_result == 2){
                players[1].balance     += _amountForWinner;
                players[1].score       += 1;
            }
        }

        // No risk of reentrance
        players[0].bet     = 0;
        players[0].move    = "";
        players[1].bet     = 0;
        players[1].move    = "";

        gameStatus = statusState.WAITING_BETS;

        emit allPreviousGames(game_outcome);


    }


    // ------------------------ FUNCTIONS ------------------------


    function checkMove(string memory _val, bool isMoveRequired)
    internal
    view
    returns(string memory)
    {
        uint152 _response = choices[_val];
        if(_response == 0){
            if(isMoveRequired == true){
                revert("Incorrect move");
            }
            return "";
        }
        return _val;
    }



}