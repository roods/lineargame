pragma solidity ^0.8.7;
// SPDX-License-Identifier: MIT
//import "hardhat/console.sol";
contract TwoPlayersGame {

    // ------------------------ STRUCTURES ------------------------

    struct player {
        address payable addr;
        uint256 bet;
        uint256 balance;
        string  move;
        uint256 score;
    }


    // ------------------------ PUBLIC ------------------------
    address public owner;

    player[2] public players;

    statusState public gameStatus;
    uint256 public lastTimestamp;

    // ------------------------ PROTECTED ------------------------

    enum boolEnum { NO, YES }
    enum statusState { CLOSED, WAITING_PLAYERS, WAITING_BETS, OPEN }

    bool internal locked = false;

    // ------------------------ EVENTS ------------------------


    // ------------------------ INIT ------------------------

//    constructor() {
//        owner = msg.sender;
//        gameStatus = statusState.WAITING_PLAYERS;
//    }

    function init()
    internal
    virtual
    {
        players[0].addr    = payable(address(0));
        players[0].balance = 0;
        players[0].bet     = 0;
        players[0].move    = "";
        players[0].score   = 0;

        players[1].addr    = payable(address(0));
        players[1].balance = 0;
        players[1].bet     = 0;
        players[1].move    = "";
        players[1].score   = 0;
    }

    // ------------------------ MODIFIERS ------------------------

    modifier _checkStatus{
        require(gameStatus != statusState.CLOSED, "The game must be opened to play");

        _;
    }

    // Verifying if user is a player
    modifier _isPlayer{
        require((players.length >= 1 && msg.sender == players[0].addr) || (players.length >= 2 && msg.sender == players[1].addr), "You must be a player");

        _;
    }

    modifier _isPlayerOrOwner{
        require((msg.sender == owner) || (players.length >= 1 && msg.sender == players[0].addr) || (players.length >= 2 && msg.sender == players[1].addr), "You must be a player");

        _;
    }



    // Preventing re-entrant exploit
    modifier noReentrant{
        require(locked == false, "Locked");
        locked = true;
        _;
        locked = false;
    }

    // ------------------------ TEST INTERFACES ------------------------

    //TODO: remove public when deploying live
//    function getPlayerInfo(address _player, string memory _attribute) public
//    view
//    returns(string memory)
//    {
//
//        require(msg.sender == owner, "You must be the owner");
//
//        for (uint i=0; i<players.length; i++) {
//            if(players[i].addr == _player){
//                if(keccak256(abi.encodePacked(_attribute)) == keccak256(abi.encodePacked("addr"))){
//                    return addrToString(players[i].addr);
//                }
//                else if(keccak256(abi.encodePacked(_attribute)) == keccak256(abi.encodePacked("bet"))){
//                    return uint2str(players[i].bet);
//                }
//                else if(keccak256(abi.encodePacked(_attribute)) == keccak256(abi.encodePacked("balance"))){
//                    return uint2str(players[i].balance);
//                }
//                else if(keccak256(abi.encodePacked(_attribute)) == keccak256(abi.encodePacked("move"))){
//                    return players[i].move;
//                }
//
//            }
//        }
//        return "";
//
//    }

    // ------------------------ INTERFACES ------------------------

    function getPlayers()
    public
    view
    returns(address payable, address payable)
    {

        return (players[0].addr, players[1].addr);
    }

    function getScores()
    public
    view
    returns(uint256, uint256)
    {
        return (players[0].score, players[1].score);
    }

    function getMyBet()
    public
    view
    returns(int256)
    {
        if(msg.sender == players[0].addr){

            return int(players[0].bet);
        }
        else if(msg.sender == players[1].addr){

            return int(players[1].bet);
        }
        return -1;
    }

    function getMyChoice()
    public
    view
    returns(string memory)
    {
        if(msg.sender == players[0].addr){

            return players[0].move;
        }
        else if(msg.sender == players[1].addr){

            return players[1].move;
        }
        return "";
    }

    // ------------------------ ACTIONS ------------------------

//    function _newGame_act()
//    internal
//    {
//        require(gameStatus == statusState.CLOSED, "You need to finish a game before starting a new one");
//
//        gameStatus = statusState.OPEN;
//    }

    function getBalance()
    public
    view
    virtual
    returns(int256)
    {

//        require(players[0].addr == msg.sender  && players[1].addr == msg.sender, "Only players can view balance");

        if(msg.sender == players[0].addr){

            return int(players[0].balance);
        }
        else if(msg.sender == players[1].addr){

            return int(players[1].balance);
        }

        return -1;

    }

    function enterGame()
    public
    virtual
    {

        require(players[0].addr != msg.sender && players[1].addr != msg.sender, "You are already in the game");

        if(players[0].addr == address(0)){

            players[0].addr    = payable(msg.sender);
            players[0].balance = 0;
            players[0].bet     = 0;
            players[0].move    = "";
            players[0].score   = 0;
        }
        else if(players[1].addr == address(0) && players[0].addr != msg.sender){

            players[1].addr    = payable(msg.sender);
            players[1].balance = 0;
            players[1].bet     = 0;
            players[1].move    = "";
            players[1].score   = 0;
        }

        if(players[0].addr != address(0) && players[1].addr != address(0)){

            gameStatus = statusState.WAITING_BETS;
        }
        else{
            gameStatus = statusState.WAITING_PLAYERS;
        }

    }

    function _placeMove_act(string memory _move)
    internal
    _checkStatus
    {
        require(players[0].addr == msg.sender || players[1].addr == msg.sender, "Only players can place a move");

        if(msg.sender == players[0].addr){
            players[0].move = _move;
        }
        else if(msg.sender == players[1].addr){
            players[1].move = _move;
        }

    }

//    function _closeGame_act()
//    internal
//    _checkStatus
//    noReentrant
//    {
//
//        require(msg.sender == owner, "You must be the owner to close the game");
//        require(gameStatus == statusState.WAITING_BETS, "The game must be finished before closing");
//
//        uint256 transfer_amount;
//        if(players[0].balance > 0){
//
//            transfer_amount = players[0].balance;
//            players[0].balance = 0;
//
//            (bool sent, ) = players[0].addr.call{value: transfer_amount}("");
//            require(sent, "Failed to send Ether");
//
//        }
//
//        if(players[1].balance > 0){
//
//            transfer_amount = players[1].balance;
//            players[1].balance = 0;
//
//            (bool sent, ) = players[1].addr.call{value: transfer_amount}("");
//            require(sent, "Failed to send Ether");
//        }
//
//        gameStatus = statusState.CLOSED;
//    }

//    function withdraw()
//    public
//    _checkStatus
//    noReentrant
//    {
//
//        uint256 transfer_amount;
//
//        if(msg.sender == players[0].addr){
//            transfer_amount = players[0].balance + players[0].bet;
//            players[0].balance = 0;
//            players[0].bet = 0;
//            players[0].move    = "";
//
//            (bool sent, ) = players[0].addr.call{value: transfer_amount}("");
//            require(sent, "Failed to send Ether");
//
//
//        }
//
//        else if(msg.sender == players[1].addr){
//            transfer_amount = players[1].balance + players[1].bet;
//            players[1].balance = 0;
//            players[1].bet = 0;
//            players[1].move    = "";
//
//            (bool sent, ) = players[1].addr.call{value: transfer_amount}("");
//            require(sent, "Failed to send Ether");
//
//        }
//
//    }

    function leaveGame()
    public
    virtual
    {

        require(msg.sender == players[0].addr || msg.sender == players[1].addr, "You must be a player to leave the game");

//        withdraw();
        uint256 transfer_amount;

        if(msg.sender == players[0].addr){

            transfer_amount = players[0].balance + players[0].bet;
            players[0].balance = 0;
            players[0].bet = 0;
            players[0].move    = "";

            (bool sent, ) = players[0].addr.call{value: transfer_amount}("");
            require(sent, "Failed to send Ether");

            players[0].addr = payable(address(0));
            players[0].score = 0;

        }

        else if(msg.sender == players[1].addr){

            transfer_amount = players[1].balance + players[1].bet;
            players[1].balance = 0;
            players[1].bet = 0;
            players[1].move    = "";

            (bool sent, ) = players[1].addr.call{value: transfer_amount}("");
            require(sent, "Failed to send Ether");

            players[1].addr = payable(address(0));
            players[1].score = 0;
        }
    }

    function betFromBalance(uint256 _amount)
    public
    _isPlayer
    virtual
    {

        require(msg.sender == players[0].addr || msg.sender == players[1].addr, "You must be a player to bet");
        require((msg.sender == players[0].addr && players[0].balance + players[0].bet >= _amount) || (msg.sender == players[1].addr && players[1].balance + players[1].bet >= _amount), "You must have enough amount in your balance to bet from it");

        if(msg.sender == players[0].addr){

            if(players[0].balance >= _amount){
                players[0].balance += players[0].bet;
                players[0].bet = _amount;
                players[0].balance -= _amount;

            }
        }
        else if(msg.sender == players[1].addr){

            if(players[1].balance >= _amount){

                players[1].balance += players[1].bet;
                players[1].bet = _amount;
                players[1].balance -= _amount;

            }
        }
    }

    function addBalance(boolEnum _useAmountToBet)
    public
    payable
    virtual
    _checkStatus
    {
        if (msg.value > msg.sender.balance) {revert();}

        if(players[0].addr != msg.sender && players[1].addr != msg.sender){
            enterGame();
        }

        require(msg.sender == players[0].addr || msg.sender == players[1].addr, "You must be a player to bet");
        require(msg.value > 0, "The bet must be greater than 0");


        if(msg.sender == players[0].addr){

            players[0].balance += msg.value;
        }
        else if(msg.sender == players[1].addr){

            players[1].balance += msg.value;
        }

        if(_useAmountToBet == boolEnum.YES){

            betFromBalance(msg.value);
        }


    }



    // ------------------------ FUNCTIONS ------------------------

    function uint2str(uint _i)
    internal
    pure
    returns (string memory str)
    {
        if (_i == 0)
        {
            return "0";
        }
        uint256 j = _i;
        uint256 length;
        while (j != 0)
        {
            length++;
            j /= 10;
        }
        bytes memory bstr = new bytes(length);
        uint256 k = length;
        j = _i;
        while (j != 0)
        {
            bstr[--k] = bytes1(uint8(48 + j % 10));
            j /= 10;
        }
        str = string(bstr);
    }

//    function st2num(string memory numString) internal pure returns(uint) {
//        uint  val=0;
//        bytes   memory stringBytes = bytes(numString);
//        for (uint  i =  0; i<stringBytes.length; i++) {
//            uint exp = stringBytes.length - i;
//            bytes1 ival = stringBytes[i];
//            uint8 uval = uint8(ival);
//            uint jval = uval - uint(0x30);
//
//            val +=  (uint(jval) * (10**(exp-1)));
//        }
//        return val;
//    }

    function addrToString(address account)
    internal
    pure
    returns(string memory)
    {
        bytes memory data = abi.encodePacked(account);

        bytes memory alphabet = "0123456789abcdef";

        bytes memory str = new bytes(2 + data.length * 2);
        str[0] = "0";
        str[1] = "x";
        for (uint i = 0; i < data.length; i++) {
            str[2+i*2] = alphabet[uint(uint8(data[i] >> 4))];
            str[3+i*2] = alphabet[uint(uint8(data[i] & 0x0f))];
        }
        return string(str);

    }
}