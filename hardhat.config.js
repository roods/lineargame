/**
 * @type import('hardhat/config').HardhatUserConfig
 */

require("@nomiclabs/hardhat-waffle");
require("./tasks/faucet");

const ROPSTEN_PRIVATE_KEY = "21aa7444c20515443b39ee84ba935f7fa5fb895124e2d620b809d5d2161a0ef2";

module.exports = {
    solidity: "0.8.7",
    networks: {
        ropsten: {
            url: `https://eth-ropsten.alchemyapi.io/v2/V74qfnSxdQFGRcE71xO6bLQ2FUOuWoeC`,
            accounts: [`0x${ROPSTEN_PRIVATE_KEY}`]
        },
        hardhat: {
            chainId: 1337
            // allowUnlimitedContractSize: true
        }
    },
    defaultNetwork: "hardhat",
};
